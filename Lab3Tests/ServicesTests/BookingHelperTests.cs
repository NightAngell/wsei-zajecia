﻿using Lab3.Models;
using Lab3.Repositories;
using Lab3.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3Tests.ServicesTests
{
    [TestFixture]
    class BookingHelperTests
    {
        Mock<IBookingRepository> _repositoryMock;
        Booking _existingBooking;

        [SetUp]
        public void SetUpBookingRepositoryMock()
        {
            _repositoryMock = new Mock<IBookingRepository>();
            _existingBooking = new Booking() {
                Id = 1,
                ArrivalDate = DateTime.Today,
                DepartureDate = After(DateTime.Today, 10),
                Reference="Ref1"
            };
            _repositoryMock.Setup(r => r.GetActiveBookings(1)).Returns(new List<Booking>
            {
                _existingBooking
            }.AsQueryable());
            BookingHelper.BookingRepository = _repositoryMock.Object;
        }

        [Test]
        public void BookingStartsAndFinishesBeforeAnExistingBooking_ReturnEmptyString()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 2,
                ArrivalDate = Before(_existingBooking.ArrivalDate, 5),
                DepartureDate = Before(_existingBooking.ArrivalDate, 2),
            });
            Assert.That(result, Is.Empty);
        }

        [Test]
        public void BookingFinishesBeforeAnExistingBookingEnd_ReturnRef()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate, 2),
                DepartureDate = Before(_existingBooking.DepartureDate),
            });
            Assert.That(result, Is.TypeOf<string>().And.Not.Empty);
        }

        [Test]
        public void BookingStartAndEndInExistingBooking_ReturnRef()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = Before(_existingBooking.DepartureDate),
            });
            Assert.That(result, Is.TypeOf<string>().And.Not.Empty);
        }

        [Test]
        public void BookingStartsWhenAnExistingBookingIsInProgress_ReturnRef()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.ArrivalDate),
                DepartureDate = After(_existingBooking.DepartureDate, 20),
            });
            Assert.That(result, Is.TypeOf<string>().And.Not.Empty);
        }

        [Test]
        public void BookingStartsAndEndAfterExistingBooking_ReturnEmptyString()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 1,
                ArrivalDate = After(_existingBooking.DepartureDate),
                DepartureDate = After(_existingBooking.DepartureDate, 20),
            });
            Assert.That(result, Is.Empty);
        }

        [Test]
        public void BookingStartsBeforeExistingBookingAndFinishesAfter_ReturnRef()
        {
            var result = BookingHelper.OverlappingBookingsExist(new Booking
            {
                Id = 1,
                ArrivalDate = Before(_existingBooking.ArrivalDate, 10),
                DepartureDate = After(_existingBooking.DepartureDate, 10),
            });
            Assert.That(result, Is.TypeOf<string>().And.Not.Empty);
        }

        private DateTime Before(DateTime dateTime, int days = 1)
        {
            return dateTime.AddDays(-days);
        }
        private DateTime After(DateTime dateTime, int days = 1)
        {
            return dateTime.AddDays(days);
        }
        private DateTime ArriveOn(int year, int month, int day)
        {
            return new DateTime(year, month, day, 14, 0, 0);
        }
        private DateTime DepartOn(int year, int month, int day)
        {
            return new DateTime(year, month, day, 10, 0, 0);
        }
    }
}
