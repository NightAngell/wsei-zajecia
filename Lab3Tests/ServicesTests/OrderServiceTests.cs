﻿using System;
using System.Text;
using System.Collections.Generic;
using NUnit.Framework;
using Moq;
using Lab3.Services;
using Lab3.Models;

namespace Lab3Tests.ServicesTests
{
    [TestFixture]
    public class OrderServiceTests
    {
        Mock<IStorage> _storageMock;

        [SetUp]
        public void StorageMockSetUp()
        {
            int id = 0;
            _storageMock = new Mock<IStorage>();
            _storageMock.Setup(storage => storage.Store(It.IsAny<Order>()))
                .Returns(() => id)
                .Callback(() => id++);
            _storageMock.Setup(s => s.Store(null)).Throws(new ArgumentNullException());
        }

        [Test]
        public void PlaceOrder_WhenCalled_StoreTheOrder()
        {
            var service = new OrderService(_storageMock.Object);
            var order = new Order();
            service.PlaceOrder(order);
            _storageMock.Verify(s => s.Store(order), Times.Once);
        }

        [Test]
        public void PlaceOrder_WhenCalled_returnIdEqual1()
        {
            var service = new OrderService(_storageMock.Object);
            service.PlaceOrder(new Order());
            int orderId = service.PlaceOrder(new Order());
            Assert.AreEqual(1, orderId);
        }

        [Test]
        public void PlaceOrder_WhenCalled_throwArgumentNullException()
        {
            var service = new OrderService(_storageMock.Object);
            Assert.Throws<ArgumentNullException>(() => service.PlaceOrder(null));
        }
    }
}
