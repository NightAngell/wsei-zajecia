﻿using Lab3.Models;
using Lab3.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Services
{
    public static class BookingHelper
    {
        private static IBookingRepository _bookingRepository = new BookingRepository();
        public static IBookingRepository BookingRepository { get => _bookingRepository; set => _bookingRepository = value; }

        public static string OverlappingBookingsExist(Booking booking)
        {
            if (booking.Status == "Cancelled")
                return string.Empty;
            var bookings = BookingRepository.GetActiveBookings(booking.Id);

            var overlappingBooking =
            bookings.FirstOrDefault(b =>
                b.ArrivalDate < booking.DepartureDate 
                && booking.ArrivalDate < b.DepartureDate
                /*booking.ArrivalDate >= b.ArrivalDate
                && booking.ArrivalDate < b.DepartureDate
                || booking.DepartureDate > b.ArrivalDate
                && booking.DepartureDate <= b.DepartureDate*/
            );
            return overlappingBooking == null ? string.Empty
            : overlappingBooking.Reference;
        }
    }
}
